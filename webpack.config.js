/*******************************************
 * BASE WEBPACK CONFIG
 * loaders:
 *   js: [ babel-loader ]
 *   shaders: [ raw-loader ] -> extensions: [ .vert, .frag ]
 * plugins: [ ExtractTextPlugin, webpack-build-notifier, UglifyJsPlugin ]
 *******************************************/

// DEPENDENCIES
const webpack  = require('webpack')
    , Notifier = require('webpack-build-notifier')
    , isProduction = process.env.NODE_ENV === "production"

/*******************************************
 * CONFIGS
 *******************************************/

const BASE_JS   = __dirname + "/app/assets/javascripts"

const JS = {
    main: [ BASE_JS + "/main.js" ],
    //main: [ __dirname + "/app/js/main.js" ]
}

const DEST_FOLDER = __dirname + "/app/assets/javascripts/dist"

const PLUGINS = []

/*******************************************
 * WEBPACK
 *******************************************/

module.exports = {
    devtool: isProduction ? 'eval' : 'source-map',
    watch: isProduction ? false : true,
    entry: JS,
    resolve: {
        extensions: ['.js', '.frag', '.vert'],
        alias: {
            'vue$': 'vue/dist/vue.common.js'
        }
    },
    module: {
        rules: [
            {
                test: /.js?$/,
                loader: 'babel-loader',
                include: BASE_JS,
                exclude: [ /node_modules/ ],
                query: {
                    presets: [
                        ['es2015', { "modules": false }]
                    ]
                }
            },
            {
                test: /\.(frag|vert)$/,
                include: BASE_JS,
                loader: "raw-loader"
            }
        ],
    },
    output: {
        filename: '[name].bundle.js',
        path: DEST_FOLDER,
    },
    plugins: PLUGINS
}

// Notifier Plugin
module.exports.plugins.push( new Notifier({ successSound: "" }) )

// UglifyJS plugin for production
if ( isProduction ) {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            minimize: true,
            compress: {
               screw_ie8: true,
               warnings: false
            }
        })
    )
}

// ADD babel polifyll to all namespaces
for ( let key in module.exports.entry ) {
    module.exports.entry[key].unshift('babel-polyfill')
}
