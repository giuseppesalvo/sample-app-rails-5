class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :set_meta

  def set_meta
    set_meta_tags og: {
                    title:        :title,
                    description:  :description,
                    type:         'website',
                    url:          :canonical,
                    image:        "#{request.base_url}/share-#{I18n.locale}.jpg"
                  },
                  title: "Sample App",
                  description: "Sample App description",
                  #site: 'websiteurl.com',
                  canonical: request.original_url
  end
end
