# RAILS 5 DUMMY APP

## HOW TO

- Clone this app
- run: $ bundle
- run: $ npm i
- change databases info in app/database.yml
- rename it with: $ rails g rename:app_to NewAwesomeName
- delete local .git folder and launch: $ git init
- When you need to make your app public, remember to delete /public/robots.txt

## Google Analytics

- Set up your code in application.html.erb


## Meta Tags

- You can change default meta tags in application_controller.erb
- If you need to change meta tags in a specific controller->action, change this global parameters
	@page_title 
	@page_description
- For other info https://github.com/kpumuk/meta-tags


## Favicons

- Put all in /public/favicons


## Images

- Put images in /public/images


## Javascript ES2015

- This application uses webpack
- if you don't have webpack, install it with: $ npm i -g webpack
- install local modules with : $ npm i
- Now, you can launch webpack
- For other info https://github.com/giuseppesalvo/webpack


## ASSETS WORKFLOW : !IMPORTANT

- If you don't follow this rules: **I will find you, and I will kill you**
- Assets are splitted in components, so EVERYTHING is a component.

**STYLESHEETS**

- Resets in settings/_resets.scss
- Components:  If you need some common styles, write it in components/_commons.scss
- Every imported file must start with an underscore for sass concatenation
- Put your static libraries in lib
- In settings you have everything you need
- Responsive: don't use media queries, but use it ( you can find all breakpoints in settings/_params.scss ):

```
@include break( medium ) {
	...
}

@include min-break( medium ) { // it starts from breakpoint + 1px -> Example: @media ( min-width: 321px )
	...
}
```
- You can define all colors in settings/_params.scss
- When you define a color, this app generates two classes for text and background color
```
.c-colorname {
	color: color(colorname);
}
.bg-colorname {
	background-color: color(colorname);
}
```

**PADDING, MARGIN AND SPACES**
- Padding is 8px, you can call it with:
```
padding: padding(); // it will be 8px
margin: padding(2); // it will be 8px * 2
```

**FONTS**
This app uses pixels, because em and rem are bad things
```
@include font( size, lineheight );
@include font( 10px, 14px );
```

**JAVASCRIPTS**

- Start webpack with: $ webpack
- Like stylesheets, everything is a component
- Put your static libraries in lib
- Put your utils in utils 
- Don't make functions, use classes, you must write good code.
- Follow this simple rules https://github.com/airbnb/javascript

